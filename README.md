# karma-winston-reporter

Reports Karma test results using [Winston](https://www.npmjs.com/package/winston).

## Installation

    npm install karma-winston-reporter --save-dev

## Usage

### From the command line

Will report results using the console transport (i.e. will do a console.log)

    karma start --reporters winston

### From karma.conf.js


    module.exports = function (config) {
        config.set({
            ...
            reporters: ['winston'],
            ...
        });
    };
    
## Message format

The reporter will make the following call after a successful test run:

    winstonLogger.log('info', {
        successfulTests: results.success,
        failedTests: results.failed,
        testDuration: results.totalTime,
        testBrowser: browser.name
    });

### Adding additional transports

    module.exports = function (config) {
        config.set({
            ...
            reporters: ['winston'],
            winstonReporter: {
                transports: [
                    new (winston.transports.Logstash)()
                ]
            }
        });
    };

## Future features

* Add support for customising message format.
* Add support for reporting individual test results.